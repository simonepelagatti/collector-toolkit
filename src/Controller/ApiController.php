<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\OpenseaService;

class ApiController extends AbstractController
{
    public function getCollection(OpenseaService $openseaService, string $slug): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'slug' => $openseaService->getCollectionStats($slug)
        ]);
    }
}
