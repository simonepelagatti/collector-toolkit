<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class OpenseaService
{
    private HttpClientInterface $client;

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client->withOptions([
            'headers' => [
                'Accept' => 'application/json',
                'X-API-KEY' => ''
            ]
        ]);
    }

    public function getCollectionStats($slug): string
    {
        $response = $this->client->request(
            'GET',
            'https://api.opensea.io/api/v1/collection/' . $slug
        );
        return $response->getContent();;
    }
}
